#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <locale.h>

#define SIZE 50

void convert(unsigned char *in, unsigned char *out)
{
    while (*in)
        if (*in < 128)
            *out++ = *in++;
        else
            *out++ = 0xc2 + (*in > 0xbf), *out++ = (*in++ & 0x3f) + 0x80;
}

int main()
{
    setlocale (LC_ALL, "") ;

    char *str = "canção";

    printf("%s\n", str);

    return 1;
}