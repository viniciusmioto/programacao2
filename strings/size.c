/* it calculates the size of  a string without strlen() */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define SIZE 20

int str_len(char *str)
{
    int i;

    for (i = 0; str[i] != '\0'; i++);

    return i;
}

int main()
{
    char name[SIZE + 1];

    fgets(name, SIZE + 1, stdin) ;
    name[strcspn (name, "\n")] = '\0' ;
    
    int str_size = str_len(name);
    printf("Size: %d\n", str_size);
    printf("%s\n", name);

    return 1;
}