/* switch Upper Case to lower case and vice-versa */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define SIZE 20

void shift_case(char *str)
{
    int i;

    for (i = 0; i < strlen(str); i++) 
    {
        if (str[i] >= 'a' && str[i] <= 'z') 
        {
            str[i] -= 32;
        }
        else if (str[i] >= 'A' && str[i] <= 'Z') 
        {
            str[i] += 32;
        }
    } 
}

int main()
{
    char str[SIZE + 1];

    fgets(str, SIZE + 1, stdin);
    str[strcspn(str, "\n")] = '\0';

    shift_case(str);

    printf("%s\n", str);

    return 1;
}