/* print a string in reverse */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define SIZE 20

int main()
{
    char name[SIZE + 1];

    fgets(name, SIZE + 1, stdin) ;
    printf("%s\n", name);

    int i;
    for (i = strlen(name); i >= 0; i--)
        printf("%c", *(name + i));
    printf("\n");

    return 1;
}