/* remove special character without an auxliar string */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define SIZE 20

void remove_char(char *str, int i)
{
    while (str[i] != '\0')
    {
        str[i] = str[i + 1];
        i++;
    }
}

void clean_string(char str[20])
{
    int i;

    i = 0;
    while (str[i] != '\0')
    {
        if (!isalpha(str[i]))
        {
            remove_char(str, i);
            clean_string(str);
        }

        i++;
    }
}

int main()
{
    char str[SIZE + 1];

    fgets(str, SIZE + 1, stdin);
    str[strcspn(str, "\n")] = '\0';

    clean_string(str);
    printf("%s\n", str);

    return 1;
}