/* sort a set of strings */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>

#define SIZE 25

void swap_strings(char str1[25], char str2[25])
{
    char temp[25];

    strcpy(temp, str1);
    strcpy(str1, str2);
    strcpy(str2, temp);
}

void sort_strings(int n, char strings[25][25])
{
    int i, j;

    for (i = 0; i <= n; i++)
    {
        for (j = i + 1; j <= n; j++)
        {
            if (strcmp(strings[i], strings[j]) > 0)
            {
                swap_strings(strings[i], strings[j]);
            }
        }
    }
}

void strlwr(char *str)
{
    int i;
    
    for (i = 0; i < strlen(str); i++) 
        if (str[i] >= 'a' && str[i] <= 'z') 
        {
            str[i] -= 32;
        }
}

int main()
{
    char strings[25][25];
    int n, i;

    printf("Quantidade de palavras: ");
    scanf("%d", &n);

    for (i = 0; i <= n; i++)
    {
        fgets(strings[i], SIZE, stdin);
        strings[i][strcspn(strings[i], "\n")] = '\0';
        strlwr(strings[i]);
    }

    sort_strings(n, strings);

    for (i = 0; i <= n; i++)
        puts(strings[i]);

    return 1;
}