/* C Program to count
/ the Number of Characters in a Text File
*/
#include <stdio.h>

#define MAX_FILE_NAME 100
#define LINESIZE 1024

int char_count(char *filename)
{
    int count = 0;
    char c;

    /* Open the file */
    FILE *fp = fopen(filename, "r");

    /* Check if file exists */
    if (fp == NULL)
    {
        printf("Could not open file %s",
               filename);
        return 0;
    }

    /* Extract characters from file
     and store in character c */
    for (c = getc(fp); c != EOF; c = getc(fp))

        /* Increment count for this character */
        count = count + 1;

    /* Close the file*/
    fclose(fp);

    return count;
}

int line_count(char *filename)
{
    int count = 0;
    char line[LINESIZE + 1];

    /* Open the file */
    FILE *fp = fopen(filename, "r");

    /* Check if file exists */
    if (fp == NULL)
    {
        printf("Could not open file %s",
               filename);
        return 0;
    }

    for (fgets(line, LINESIZE, fp); !feof(fp); fgets(line, LINESIZE, fp))

        /* Increment count for this character */
        count = count + 1;

    /* Close the file */
    fclose(fp);

    return ++count;
}

int main()
{
    /* Character counter (result) */
    int lines, characters;

    char filename[MAX_FILE_NAME];

    /* Get file name from user.
    / The file should be either in current folder
    / or complete path should be provided
    */
    printf("Enter file name: ");
    scanf("%s", filename);

    characters = char_count(filename);
    lines = line_count(filename);

    /* Print the count of characters */
    printf("The file %s has %d characters and %d lines\n ",
           filename, characters, lines);

    return 0;
}
