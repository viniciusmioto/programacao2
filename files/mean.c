/* C Program to count
 the Number of Characters in a Text File
*/

#include <stdio.h>

#define MAX_FILE_NAME 100
#define LINESIZE 1024

float mean(char *filename)
{
    /* number of lines in the file */
    int quantity;
    char line[LINESIZE + 1];
    float sum, value;

    quantity = 0;
    sum = 0;

    /* Open the file */
    FILE *fp = fopen(filename, "r");

    /* Check if file exists */
    if (fp == NULL)
    {
        printf("Could not open file %s",
               filename);
        return 0;
    }

    do
    {
        fscanf (fp, "%f", &value);
        sum += value;
        quantity++;

        fgets(line, LINESIZE, fp);
    } while (!feof(fp));
    

    /* Close the file */
    fclose(fp);

    return sum/quantity;
}

int main()
{
    char filename[MAX_FILE_NAME];

    /* Get file name from user.
    / The file should be either in current folder
     or complete path should be provided */
    printf("Enter file name: ");
    scanf("%s", filename);

    float mean_value = mean(filename);

    printf("Mean = %0.2f\n", mean_value);

    return 0;
}
