#include <stdio.h>

void print_array(int *array)
{
    int i;

    for (i = 0; i < 10; i++)
    {
        printf("%d ", *array);
        array++;
    }
    printf("\n");
}

void swap(int *a, int *b)
{
    int aux;

    aux = *a;
    *a = *b;
    *b = aux;
}

void read_array(int *array)
{
    int i;

    for (i = 0; i < 10; i++)
    {
        scanf("%d", array);
        array++;
    }
}

void bubble_sort(int *array)
{
    int i, j;

    for (i = 9; i > 0; i--)
    {
        for (j = 0; j < i; j++)
        {
            if (*(array+j) > *(array+(j+1)))
            {
                swap((array+j), (array+j+1));
            }
        }
    }
}

int main ()
{
    int array[10] = {};

    read_array(array);
    print_array(array);
    bubble_sort(array);
    print_array(array);

    return 0;
}