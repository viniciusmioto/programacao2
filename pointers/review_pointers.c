#include <stdio.h>

int read_integers(int *pointer)
{
    int number;
    printf("Enter a number: ");
    scanf("%d", &number);
    pointer = &number;
    printf("number: %d\n", number);
    printf("*pointer: %d\n", *pointer);
    printf("&number: %p\n", &number);
    printf("pointer: %p\n", pointer);
    return 1;
}

int main ()
{
    int *pointer;
    read_integers(pointer);
    return 0;
}