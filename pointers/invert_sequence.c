#include <stdio.h>

int main ()
{
    int array[10] = {};
    int *pointer;
    int i;

    pointer = array;

    for (i = 0; i < 10; i++)
    {
        scanf("%d", pointer);
        pointer++;
    }

    printf("\n");

    for (i = 0; i < 10; i++)
    {
        pointer--;
        printf("%d ", *pointer);
    }

    printf("\n");

    return 0;
}