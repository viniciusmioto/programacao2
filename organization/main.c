#include <stdio.h>
#include "sum.h"

// read and sum two numbers
int main () {
    int a, b;

    printf("Enter two numbers: ");
    scanf("%d %d", &a, &b);

    printf("Sum: %d\n", sum(a, b));

    return 0;
}