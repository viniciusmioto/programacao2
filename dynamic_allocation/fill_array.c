#include <stdio.h>
#include <stdlib.h>

typedef struct Node
{
    int value;
    struct Node *next;
} Node;

typedef struct List
{
    Node *head;
} List;

int empty_list(List *list)
{
    return list->head == NULL;
}

void init_list(List *list)
{
    list->head = NULL;
}

int insert_first(int x, List *list)
{
    if (!empty_list(list))
        return 0;

    Node *new_node = malloc(sizeof(Node));

    if (new_node == NULL)
        return 0;

    new_node->value = x;
    new_node->next = list->head;
    list->head = new_node;

    return 1;
}

int append(int x, List *list)
{
    if (empty_list(list))
        return insert_first(x, list);

    Node *current = list->head;
    Node *new_node = malloc(sizeof(Node));

    if (new_node == NULL)
        return 0;

    new_node->next = NULL;
    new_node->value = x;

    while (current->next != NULL)
        current = current->next;

    current->next = new_node;

    return 1;
}

int fill_list(List *list, int n)
{
    int i;

    for (i = 0; i < n; i++)
        append(i * 100, list);
    
    return 1;
}

int deallocate_list(List *list)
{
    Node *current;

    if (empty_list(list))
        return 0;

    while (list->head->next != NULL)
    {
        current = list->head;
        list->head = list->head->next;
        free(current);
    }

    free(list->head);
    list->head = NULL;
    
    return 1;
    
}

void print_list(List list)
{
    if (empty_list(&list))
        return;

    Node *current;

    for (current = list.head; current != NULL; current = current->next)
        printf("%d ", current->value);

    printf("\n");
}

int main()
{
    int n;
    List list;
    
    printf("Informe o tamanho do vetor: ");
    scanf("%d", &n);

    init_list(&list);
    fill_list(&list, n);
    print_list(list);
    deallocate_list(&list);
    print_list(list);
    return 1;
}
