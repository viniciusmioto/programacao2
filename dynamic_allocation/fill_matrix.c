#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define ROW 4
#define COL 6

void* safe_malloc (unsigned long size)
{
  void* ptr ;
 
  /* alloc memory with the parameter */
  ptr = malloc (size) ;
 
  /* allocation test */
  if (! ptr)
  {
    fprintf (stderr, "malloc of %ld bytes failed\n", size) ;
    exit (1) ;
  }
 
  /* set memory section with 0s */
  memset (ptr, 0, size);
 
  return (ptr) ;
}


void print_matrix(int *matrix)
{
    int i, j;

    for (i = 0; i < ROW; i++)
    {
        for (j = 0; j < COL; j++)
            printf("%d ", matrix[(i * COL) + j]);
        printf("\n");
    }
}

int main()
{
    int *matrix;
    int i, j;

    matrix = safe_malloc(ROW * COL * sizeof(int));

    for (i = 0; i < ROW; i++)
        for (j = 0; j < COL; j++)
            matrix[(i * COL) + j] = i + j;

    print_matrix(matrix);
    free(matrix);

    return 1;
}